var express = require('express');
var app = express();
var router = express.Router();
var mysql = require('mysql');
var bodyParser = require('body-parser');


var connection = mysql.createConnection({
  host     : '208.91.199.11',
  user     : 'appdbuser',
  password : 'Admin@4321',
	database : 'application_db',
	port:'3306'
});

connection.connect(function(err) {
  if (err) throw err
  console.log('You are now connected with mysql database...')
})
//end mysql connection

//start body-parser configuration
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
//end body-parser configuration

//create app server
var server = app.listen(3000,  "127.0.0.1", function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)

});

app.get('/city', function (req, res) {
	connection.query('select * from lkp_city', function (error, results, fields) {
	 if (error) throw error;
	 res.end(JSON.stringify(results));
 });
});

app.post('/city/add/:city',function(req,res){
	console.log("===========>",req.body)
	res.end(JSON.stringify({code:200}));
})
